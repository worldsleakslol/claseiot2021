## Informe de seguridad para ASUS RT-AC1750

Tras la auditoría, se ha detectado que el sistema se alojaba en un router con modelo RT-AC170 de la marca ASUS y ha resultado ser muy inseguro por diversos motivos.

El sistema es aparentemente vulnerable a la vulnerabilidad con CVE-2013-4937, calificada con la máxima puntuación en cuanto a riesgo además de requerir apenas conocimientos técnicos para su ejecución. La vulnerabilidad conlleva una exposición de información crítica en texto plano, fácilmente accesible, en aquellos sistemas con versión inferior a 3.0.4.372. Debido a que el sistema a auditar tiene una versión 2.0.2.3 queda expuesto a esta vulnerabilidad crítica.

Las categorías de OWASP que se han analizado son:

- Contraseñas inseguras: Encontramos contraseñas inseguras y en texto plano. La mayoría de ellas son fácilmente comprometidas con diccionarios ya que no contienen más de 8 caracteres alfanuméricos. 
- Privacidad insuficiente: Somos capaces de listar diversos usuarios del sistema con relativa facilidad.
- Tratamiento de datos inseguro: Durante la auditoría encontramos un fichero de texto con información crítica donde quedaban expuestas las credenciales de los usuarios del sistema.
- Configuración por defecto insegura: Encontramos archivos de configuración del router donde se exponen en texto plano credenciales por defecto. 

Las recomendaciones de seguridad son las siguientes:

- Establecer una política de contraseñas estricta que controle una longitud y complejidad mínima. 
- No almacenar ficheros en texto plano que puedan comprometer la seguridad del sistema con información crítica como credenciales.
- Cambiar las credenciales por defecto asap. 
- Mantener una estricta rutina de control e instalación de actualizaciones.
- Mantenerse informado de las vulnerabilidades que vayan apareciendo que puedan comprometer nuestros sistemas. Para ello, podemos visitar páginas como Shodan, INCIBE, MITRE etc.

Para concretar la información crítica expuesta, contactar con el auditor.


