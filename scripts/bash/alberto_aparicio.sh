##############################################################
# alberto_aparicio.sh
# Written by Alberto Aparicio (Worldsleaks)
# Ask for a path to execute 'firmwalker' and 
# save the output in a text file.
##############################################################

#!/bin/bash

read -p "Enter the path: " path
read -p "Where do i save the output?: " file
cd /home/worldsleaks/Auditoria/firmwalker
./firmwalker.sh $path > $file 2> /dev/null
echo "Done!"
