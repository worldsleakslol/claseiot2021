##############################################################
# alberto_aparicio.py
# Written by Alberto Aparicio (Worldsleaks)
# Reads a given file and
# counts how many times the string '.pem' appears.
##############################################################

import os

file_name = input("Enter the file: ");

#Check if the file exists:
if os.path.isfile(file_name):
        f = open(file_name, "r", encoding="utf8");
        content=f.readlines()
        f.close()
        
        recount_total=0
        for lines in content:
                line=lines.strip("\n")
                if line.count(".pem")>0:
                        recount_total=recount_total+line.count(".pem")

        print("The file extension '.pem' appears",recount_total-1,"times.")
        
else:
        print(f"The file '{file_name}' doesn't exist.")

